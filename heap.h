#ifndef HEAP_H
#define HEAP_H
#define max 100

class heap
{
public:
    heap();
    ~heap();
    void agregar(int, int);
    int extraer();
    bool vacio();

private:
    class DatoPrivado
    {
    public:
        int prioridad;
        int dato;
    };
    DatoPrivado vector[100];
    int ultimo;
    void subir();
    void bajar();
};
#endif