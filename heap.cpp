#include "heap.h"

heap::heap()
{
    ultimo = 0;
}

heap::~heap()
{
}

//metodo para intercambiar si el padre es mayor
void heap::subir()
{
    int padre, pos;
    DatoPrivado hijo;

    hijo = vector[ultimo];
    vector[0] = vector[ultimo];
    pos = ultimo;    //ultima posicion
    padre = pos / 2; // posicion del padre

    //mientras el padre se mayor que el hijo
    while (vector[padre].prioridad > hijo.prioridad)
    {
        //cambio de posicion del hijo y padre
        vector[pos] = vector[padre];

        pos = padre;     //Posicion del padre
        padre = pos / 2; //Posicion del nuevo padre
    }
    vector[pos] = hijo; // nueva posicion del padre
}

void heap::bajar()
{
    int k, i;
    bool fin;
    DatoPrivado aux;

    aux = vector[1];
    k = 1;
    i = k * 2;
    fin = false;

    while (k <= ultimo / 2 && !fin)
    {
        if (i < ultimo)
        {
            if (vector[i + 1].prioridad < vector[i].prioridad)
            {
                i++;
            }
        }
        if (aux.prioridad > vector[i].prioridad)
        {
            vector[k] = vector[i];
            k = i;
            i = k * 2;
        }
        else
        {
            fin = true;
        }
    }
    vector[k] = aux;
}

void heap::agregar(int dato, int prioridad)
{
    DatoPrivado aux;

    aux.prioridad = prioridad;
    aux.prioridad = dato;
    ultimo = ultimo + 1;
    vector[ultimo] = aux;
    subir();
}

int heap::extraer()
{
    int x = vector[1].dato;
    vector[1] = vector[ultimo];
    ultimo = ultimo - 1;
    bajar();
    return x;
}
bool heap::vacio()
{
    return (ultimo == 0);
}
